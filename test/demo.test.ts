import { JsonSchema } from '../src';

it('listen values', () => {
  const myStructure = {
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
    s: 'snake',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [1, 2, 3],
    products: [
      { name: 'cup', price: 4 },
      { name: 'glass jar', price: 10 },
      { name: 'spoon', price: 1 },
    ],
  };

  const schema = new JsonSchema()
    .listenValue(['a'], ({ value }) => {
      expect(value).toBe('alpaca');
    })
    .listenValue(['object', 'foo'], ({ value }) => {
      expect(value).toBe(1);
    })
    .listenValue(['array'], ({ value }) => {
      expect([1, 2, 3]).toContain(value); // each array element
    })
    .listenValue(['products', 'name'], ({ value }) => {
      expect(['cup', 'glass jar', 'spoon']).toContain(value); // name of each array element
    });

  schema.triggerListeners(myStructure);
});

it('listen fields', () => {
  const myStructure = {
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
    s: 'snake',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [1, 2, 3],
    products: [
      { name: 'cup', price: 4 },
      { name: 'glass jar', price: 10 },
      { name: 'spoon', price: 1 },
    ],
  };

  const schema = new JsonSchema()
    .listenField(['array'], ({ value }) => {
      expect(value).toEqual([1, 2, 3]); // array
    })
    .listenField(['products'], ({ value }) => {
      // array
      expect(value).toEqual([
        { name: 'cup', price: 4 },
        { name: 'glass jar', price: 10 },
        { name: 'spoon', price: 1 },
      ]);
    });

  schema.triggerListeners(myStructure);
});

it('alter values', () => {
  const myStructure = {
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
    s: 'snake',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [1, 2, 3],
    products: [
      { name: 'cup', price: 4 },
      { name: 'glass jar', price: 10 },
      { name: 'spoon', price: 1 },
    ],
  };

  const schema = new JsonSchema()
    .alterValue(['c'], ({ value }) => `${value}-case`)
    .alterValue(['s'], ({ value }) => `${value}Case`)
    .alterValue(['array'], ({ value }) => value * 2)
    .alterValue(['products', 'name'], ({ value }) => (value as string).toUpperCase());

  expect(schema.triggerAlterators(myStructure)).toEqual({
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel-case',
    s: 'snakeCase',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [2, 4, 6],
    products: [
      { name: 'CUP', price: 4 },
      { name: 'GLASS JAR', price: 10 },
      { name: 'SPOON', price: 1 },
    ],
  });
});

it('alter fields', () => {
  const myStructure = {
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
    s: 'snake',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [1, 2, 3],
    products: [
      { name: 'cup', price: 4 },
      { name: 'glass jar', price: 10 },
      { name: 'spoon', price: 1 },
    ],
  };

  const schema = new JsonSchema()
    .alterField(['array'], ({ value }) => {
      return value.filter((el: number) => el > 1);
    })
    .alterField(['products'], ({ value }) => {
      return (value as any[]).map(el => ({
        name: el.name,
        price: `$${el.price}.00`,
      }));
    });

  expect(schema.triggerAlterators(myStructure)).toEqual({
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
    s: 'snake',
    object: {
      foo: 1,
      bar: 2,
    },
    array: [2, 3],
    products: [
      { name: 'cup', price: '$4.00' },
      { name: 'glass jar', price: '$10.00' },
      { name: 'spoon', price: '$1.00' },
    ],
  });
});

it('nested schema', () => {
  const upperSchema = new JsonSchema()
    .alterField([], ({ value }) => value.toUpperCase());
  const trimSchema = new JsonSchema()
    .alterField(['field'], ({ value }) => value.trim())
    .append(['field'], upperSchema);

  const field = ' I am a cat ';
  const myObject = { field };

  const upperResult = upperSchema.triggerAlterators(field);
  expect(upperResult).toBe(' I AM A CAT ');

  const trimResult = trimSchema.triggerAlterators(myObject);
  expect(trimResult).toEqual({ field: 'I AM A CAT' });
});
