import { JsonSchema } from '../src';

it('root alter', () => {
  const schema = new JsonSchema();

  schema.alterValue([], ({ value }) => {
    return value * 2;
  });

  const result = schema.triggerAlterators(10);
  expect(result).toBe(20);
});

it('alter one level fields value', () => {
  const schema = new JsonSchema();

  schema.alterValue(['name'], ({ value }) => {
    return (value as string).toUpperCase();
  });
  schema.alterValue(['price'], ({ value }) => {
    return `$${value}`;
  });

  const result = schema.triggerAlterators({
    name: 'xiaomi redmi note 8 pro',
    price: 5600,
  });

  expect(result).toEqual({
    name: 'XIAOMI REDMI NOTE 8 PRO',
    price: '$5600',
  });
});

it('alter two levels fields value', () => {
  const schema = new JsonSchema();

  const places: any = {
    tx: 'Texas',
    ny: 'New York',
  };

  schema.alterValue(['summary', 'total'], ({ value }) => {
    return `$${value}`;
  });
  schema.alterValue(['trip', 'origin'], ({ value }) => {
    return places[value];
  });
  schema.alterValue(['trip', 'destination'], ({ value }) => {
    return places[value];
  });

  const result = schema.triggerAlterators({
    summary: {
      total: 1500,
    },
    trip: {
      origin: 'tx',
      destination: 'ny',
    },
  });

  expect(result).toEqual({
    summary: {
      total: '$1500',
    },
    trip: {
      origin: 'Texas',
      destination: 'New York',
    },
  });
});

it('alter array values', () => {
  const schema = new JsonSchema();
  schema.alterValue([], ({ value }) => {
    return value * 3;
  });

  const result = schema.triggerAlterators([1, 2, 3]);
  expect(result).toEqual([ 3, 6, 9]);
});

it('alter array field', () => {
  const schema = new JsonSchema();
  schema.alterField([], ({ value }) => {
    return value.map((l: string) => l.toUpperCase()).join(' ');
  });

  const result = schema.triggerAlterators(['w', 'h', 'a', 't', '?']);
  expect(result).toBe('W H A T ?');
});

it('alter appended fields and array fields value', () => {
  const movementSchema = new JsonSchema();
  movementSchema.alterValue(['description'], ({ value }) => {
    return `${value.slice(0, 1).toUpperCase()}${value.slice(1)}`;
  });

  const summarySchema = new JsonSchema();
  summarySchema.alterValue([], ({ parent: { movements } }) => ({
    movements: movements.length,
    egresses: movements.filter((mov: any) => mov.amount < 0).length,
    incomes: movements.filter((mov: any) => mov.amount > 0).length,
  }));

  const rootSchema = new JsonSchema();
  rootSchema.append(['movements'], movementSchema);
  rootSchema.append(['summary'], summarySchema);

  const result = rootSchema.triggerAlterators({
    movements: [
      {
        description: 'card payment',
        amount: -200,
      },
      {
        description: 'wire transfer',
        amount: 22000,
      },
      {
        description: 'other',
        amount: -500,
      },
    ],
    summary: {},
  });

  expect(result).toEqual({
    movements: [
      {
        description: 'Card payment',
        amount: -200,
      },
      {
        description: 'Wire transfer',
        amount: 22000,
      },
      {
        description: 'Other',
        amount: -500,
      },
    ],
    summary: {
      movements: 3,
      egresses: 2,
      incomes: 1,
    },
  });
});

it('some field alterators', () => {
  const schema = new JsonSchema()
    .alterValue(['text'], ({ value }) => value.toUpperCase())
    .alterValue(['text'], ({ value }) => value.split(''))
    .alterValue(['text'], ({ value }) => value.join(' '));

  const result = schema.triggerAlterators({
    text: 'little',
  });

  expect(result).toEqual({
    text: 'L I T T L E',
  });
});

it('recursive alter', () => {
  const nonPiramidalScheme = new JsonSchema();
  nonPiramidalScheme.alterValue(['name'], ({ value }) => value.toUpperCase());
  nonPiramidalScheme.alterField(['guests'], ({ value }) => {
    return value.filter((guest: any) => !guest.name.includes('Electric'));
  });
  nonPiramidalScheme.append(['guests'], nonPiramidalScheme);

  // https://www.goodhousekeeping.com/life/parenting/g3183/celebrity-baby-names/
  const alteredValue = nonPiramidalScheme.triggerAlterators({
    name: 'X Æ A-12 Musk',
    guests: [
      {
        name: 'Slash Electric',
        guests: [
          { name: 'Banks Violet' },
          { name: 'Gravity Blue' },
        ],
      },
      {
        name: 'Story Grey',
      },
      {
        name: 'Beatrice Danger',
        guests: [
          { name: 'Navy Rome' },
        ],
      },
    ],
  });

  expect(alteredValue).toEqual({
    name: 'X Æ A-12 MUSK',
    guests: [
      {
        name: 'STORY GREY',
      },
      {
        name: 'BEATRICE DANGER',
        guests: [
          { name: 'NAVY ROME' },
        ],
      },
    ],
  });
});

it('alter nested schema fields', () => {
  const productSchema = new JsonSchema()
    .alterField([], ({ value: product }) => ({
      ...product,
      total: product.price * 1.16, // Mexican taxes
    }));

  const purchaseSchema = new JsonSchema()
    .alterField(['products'], ({ value: products }) => {
      return products.filter((prod: any) => prod.price > 0);
    })
    .append(['products'], productSchema);

  const alteredValue = purchaseSchema.triggerAlterators({
    products: [
      { price: 0 },
      { price: 10 },
      { price: 20 },
    ],
  });

  expect(alteredValue).toEqual({
    products: [
      { price: 10, total: 11.6 },
      { price: 20, total: 23.2 },
    ],
  });
});
