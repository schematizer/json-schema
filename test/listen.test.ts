import { JsonSchema } from '../src';

it('root listen', () => {
  let evaluated: boolean = false;
  const schema = new JsonSchema();

  schema.listenValue([], ({ value }) => {
    expect(value).toBe(10);
    evaluated = true;
  });
  schema.triggerListeners(10);

  expect(evaluated).toBeTruthy();
});

it('listen one level fields value', () => {
  let evaluations = 0;
  const schema = new JsonSchema();

  schema.listenValue(['a'], ({ value }) => {
    expect(value).toBe('alpaca');
    evaluations++;
  });

  schema.listenValue(['b'], ({ value }) => {
    expect(value).toBe('buffalo');
    evaluations++;
  });

  schema.listenValue(['c'], ({ value }) => {
    expect(value).toBe('camel');
    evaluations++;
  });

  schema.triggerListeners({
    a: 'alpaca',
    b: 'buffalo',
    c: 'camel',
  });
  expect(evaluations).toBe(3);
});

it('listen two levels fields value', () => {
  let evaluations = 0;
  const schema = new JsonSchema();

  schema.listenValue(['a', 'foo'], ({ value }) => {
    expect(value).toBe(1);
    evaluations++;
  });
  schema.listenValue(['a', 'bar'], ({ value }) => {
    expect(value).toBe(2);
    evaluations++;
  });
  schema.listenValue(['b'], ({ value }) => {
    expect(value).toEqual({ foo: 1, bar: 2 });
    evaluations++;
  });
  schema.listenValue(['b', 'bar'], ({ value }) => {
    expect(value).toBe(2);
    evaluations++;
  });

  schema.triggerListeners({
    a: {
      foo: 1,
      bar: 2,
    },
    b: {
      foo: 1,
      bar: 2,
    },
  });
  expect(evaluations).toBe(4);
});

it('listen appended fields value', () => {
  let evaluations = 0;

  const rectangleSchema = new JsonSchema();
  rectangleSchema.listenValue(['width'], ({ value }) => {
    expect(value).toBe(10);
    evaluations++;
  });
  rectangleSchema.listenValue(['height'], ({ value }) => {
    expect(value).toBe(5);
    evaluations++;
  });

  const trapezoidSchema = new JsonSchema();
  trapezoidSchema.listenValue(['shortBase'], ({ value }) => {
    expect(value).toBe(15);
    evaluations++;
  });
  trapezoidSchema.listenValue(['longBase'], ({ value }) => {
    expect(value).toBe(20);
    evaluations++;
  });
  trapezoidSchema.listenValue(['height'], ({ value }) => {
    expect(value).toBe(10);
    evaluations++;
  });

  const figuresSchema = new JsonSchema();
  figuresSchema.append(['rectangle'], rectangleSchema);
  figuresSchema.append(['trapezoid'], trapezoidSchema);

  figuresSchema.triggerListeners({
    rectangle: {
      width: 10,
      height: 5,
    },
    trapezoid: {
      shortBase: 15,
      longBase: 20,
      height: 10,
    },
  });

  expect(evaluations).toBe(5);
});

it('listen array values', () => {
  const schema = new JsonSchema();

  const values = [1, 2, 3, {}, '', [1, 2, 3]];
  schema.listenValue([], ({ value }) => {
    const [expectedValue] = values.splice(0, 1);
    expect(value).toBe(expectedValue);
  });
  schema.triggerListeners([...values]);

  expect(values).toEqual([]);
});

it('listen fields value in array as root', () => {
  const brands = ['ferrari', 'ford'];
  const agencyNames = [
    'Laredo agency',
    'Erupean cars',
    'Highway agency',
    'Plus cars',
  ];

  const schema = new JsonSchema();
  schema.listenValue(['agencies', 'name'], ({ value }) => {
    const [agencyName] = agencyNames.splice(0, 1);
    expect(value).toBe(agencyName);
  });
  schema.listenValue(['brand'], ({ value }) => {
    const [brand] = brands.splice(0, 1);
    expect(value).toBe(brand);
  });

  schema.triggerListeners([
    {
      brand: 'ferrari',
      agencies: [
        { name: 'Laredo agency' },
        { name: 'Erupean cars' },
      ],
    },
    {
      brand: 'ford',
      agencies: [
        { name: 'Highway agency' },
        { name: 'Plus cars' },
      ],
    },
  ]);

  expect(brands).toEqual([]);
  expect(agencyNames).toEqual([]);
});

it('listen nested fields value in arrays', () => {
  const names = ['Nova', 'Kiki'];

  const schema = new JsonSchema();
  schema.listenValue(['pets', 'name'], ({ value }) => {
    const [name] = names.splice(0, 1);
    expect(value).toBe(name);
  });

  schema.triggerListeners({
    pets: [
      { name: 'Nova' },
      { name: 'Kiki' },
    ],
  });

  expect(names).toEqual([]);
});

it('listen field', () => {
  let evaluated = false;
  const schema = new JsonSchema();

  schema.listenField(['numbers'], ({ value }) => {
    evaluated = true;
    expect(value).toEqual([1, 2, 3]);
  });
  schema.triggerListeners({
    numbers: [1, 2, 3],
  });

  expect(evaluated).toBeTruthy();
});

it('some field listeners', () => {
  let calls = 0;
  const schema = new JsonSchema()
    .listenValue(['foo'], ({ value }) => {
      expect(value).toBe('bar');
      calls++;
    })
    .listenValue(['foo'], ({ value }) => {
      expect(value).toBe('bar');
      calls++;
    });

  schema.triggerListeners({ foo: 'bar' });
  expect(calls).toBe(2);
});

it('recursive listeners', () => {
  const names: string[] = [];

  const schema = new JsonSchema();
  schema.listenValue(['name'], ({ value }) => names.push(value));
  schema.append(['children'], schema);

  schema.triggerListeners({
    name: 'personal folder',
    children: [
      {
        name: 'notes.txt',
      },
      {
        name: 'images',
        children: [
          { name: 'o rly.jpg' },
          { name: 'troll face.jpg' },
        ],
      },
      {
        name: 'is not porn',
        children: [
          { name: 'learn c++.lnk' },
        ],
      },
    ],
  });

  expect(names).toEqual([
    'personal folder',
    'notes.txt',
    'images',
    'o rly.jpg',
    'troll face.jpg',
    'is not porn',
    'learn c++.lnk',
  ]);
});
