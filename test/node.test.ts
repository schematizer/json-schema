import { JsonSchema } from '../src';

it('get root', () => {
  const schema = new JsonSchema();
  expect(schema.node([])).toBe(schema.root);
});

it('create and get one level nodes', () => {
  const schema = new JsonSchema();
  const aNode = schema.node(['a']);
  const bNode = schema.node(['b']);

  const nodes: any = schema.root.nodes;
  if (!nodes) throw new Error('imposible case');

  expect(aNode).toBe(nodes.a);
  expect(bNode).toBe(nodes.b);
  expect(aNode).toBe(schema.node(['a']));
  expect(bNode).toBe(schema.node(['b']));

  expect(schema.root).toEqual(
    expect.objectContaining({
      nodes: {
        a: expect.any(Object),
        b: expect.any(Object),
      },
    }),
  );
});

it('create and get two levels nodes', () => {
  const schema = new JsonSchema();
  const a1Node = schema.node(['a', '1']);
  const a2Node = schema.node(['a', '2']);
  const aNode = schema.node(['a']);
  const bNode = schema.node(['b']);
  const b1Node = schema.node(['b', '1']);
  const b2Node = schema.node(['b', '2']);

  const nodes: any = schema.root.nodes;

  expect(aNode).toBe(nodes.a);
  expect(a1Node).toBe(schema.node(['a', '1']));
  expect(a2Node).toBe(schema.node(['a', '2']));

  expect(bNode).toBe(nodes.b);
  expect(b1Node).toBe(schema.node(['b', '1']));
  expect(b2Node).toBe(schema.node(['b', '2']));

  expect(schema.root).toEqual(
    expect.objectContaining({
      nodes: {
        a: expect.objectContaining({
          nodes: {
            1: expect.any(Object),
            2: expect.any(Object),
          },
        }),
        b: expect.objectContaining({
          nodes: {
            1: expect.any(Object),
            2: expect.any(Object),
          },
        }),
      },
    }),
  );
});
