import { NodeAlter, NodeListener } from '../types';

export class Node<Context = any> {
  public appended: Node[] = [];
  public nodes: Record<string, Node<Context>> = {};
  public listenValue: Array<NodeListener<Context>> = [];
  public listenField: Array<NodeListener<Context>> = [];
  public alterValue: Array<NodeAlter<Context>> = [];
  public alterField: Array<NodeAlter<Context>> = [];
}
