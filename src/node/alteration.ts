import { ListenerArg, NodeAlter } from '../types';
import { Node } from './Node';

export interface NodeAlterationArg {
  context: any;
  node: Node<any>;
  path: string[];
  parentValue: any;
  setValue: (value: any) => void;
  value: any;
}

function triggerAlterers(alterators: Array<NodeAlter<any>>, args: ListenerArg<any>) {
  return alterators.reduce((prevResult, alterator) => {
    return alterator({
      ...args,
      value: prevResult,
    });
  }, args.value);
}

export function alterNodeValue({
  context,
  node,
  path,
  parentValue,
  setValue,
  value: initialValue,
}: NodeAlterationArg) {
  const { nodes } = node;
  let value = initialValue;
  for (const appended of node.appended) {
    alterNodeField({
      context,
      node: appended,
      parentValue,
      path,
      setValue: newValue => {
        setValue(newValue);
        value = newValue;
      },
      value,
    });
  }

  if (typeof value === 'object') {
    // iterate over the children nodes
    for (const name of Object.keys(nodes)) {
      const childNode = nodes[name];
      const hasField = name in value;
      if (!hasField) continue;

      alterNodeField({
        context,
        node: childNode,
        path: [...path, name],
        parentValue: value,
        setValue: newValue => value[name] = newValue,
        value: value[name],
      });
    }
  }

  if (node.alterValue.length) {
    const newValue = triggerAlterers(node.alterValue, {
      context,
      path,
      value,
      parent: parentValue,
    });
    setValue(newValue);
  }
}

export function alterNodeField({
  context,
  node,
  path,
  parentValue,
  setValue,
  value,
}: NodeAlterationArg) {
  let fieldValue = value;
  if (node.alterField.length) {
    const alteredFieldValue = triggerAlterers(node.alterField, {
      context,
      parent: parentValue,
      path,
      value: fieldValue,
    });
    setValue(alteredFieldValue);
    fieldValue = alteredFieldValue;
  }

  if (fieldValue instanceof Array) {
    fieldValue.forEach((elementValue, index) => alterNodeValue({
      context,
      node,
      path,
      parentValue,
      value: elementValue,
      setValue: newValue => fieldValue[index] = newValue,
    }));
  } else {
    alterNodeValue({
      context,
      node,
      path,
      parentValue,
      value: fieldValue,
      setValue,
    });
  }
}
