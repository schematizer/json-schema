import { ListenerArg, NodeListener } from '../types';
import { Node } from './Node';

export interface NodeIterationArg {
  context: any;
  node: Node<any>;
  value: any;
  parentValue: any;
  path: string[];
}

function triggerListeners(listeners: Array<NodeListener<any>>, args: ListenerArg<any>) {
  listeners.forEach(listener => listener(args));
}

function iterateNodeValue({
  context,
  node,
  parentValue,
  value,
  path,
}: NodeIterationArg) {
  // trigger listener
  triggerListeners(node.listenValue, {
    context,
    path,
    value,
    parent: parentValue,
  });

  // join to appended nodes
  for (const appended of node.appended) {
    iterateNodeField({
      context,
      node: appended,
      parentValue,
      path,
      value,
    });
  }

  // validate if value is an object
  const { nodes } = node;
  if (typeof value !== 'object') return;

  // iterate over the children nodes
  for (const name of Object.keys(nodes)) {
    const childNode = nodes[name];
    const hasField = name in value;
    if (!hasField) continue;

    iterateNodeField({
      context,
      node: childNode,
      parentValue: value,
      path: [...path, name],
      value: value[name],
    });
  }
}

export function iterateNodeField({
  context,
  node,
  parentValue,
  value: fieldValue,
  path,
}: NodeIterationArg) {
  if (node.listenField.length) {
    triggerListeners(node.listenField, {
      context,
      parent: parentValue,
      path,
      value: fieldValue,
    });
  }

  if (fieldValue instanceof Array) {
    fieldValue.forEach(value => iterateNodeValue({
      context,
      node,
      parentValue: fieldValue,
      path,
      value,
    }));
  } else {
    iterateNodeValue({
      context,
      node,
      parentValue,
      path,
      value: fieldValue,
    });
  }
}
