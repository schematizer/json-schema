import { Node } from './Node';

export function* nodeGenerator(
  root: Node<any>,
): Generator<Node<any>, void, string> {
  let node = root;
  const path: string[] = [];

  while (true) {
    const next = yield node;
    path.push(next);

    // create nodes container
    if (!node.nodes) {
      node.nodes = {};
    }

    // create node
    if (!node.nodes[next]) {
      node.nodes[next] = new Node();
    }

    // set new current node
    node = node.nodes[next];
  }
}
