import { alterNodeField, iterateNodeField, Node, nodeGenerator } from './node';
import { NodeAlter, NodeListener } from './types';

export class JsonSchema<Context> {
  public root = new Node<Context>();

  public listenValue(path: string[], listener: NodeListener<Context>) {
    const node = this.node(path);
    if (!node.listenValue) {
      node.listenValue = [];
    }
    node.listenValue.push(listener);
    return this;
  }

  public listenField(path: string[], listener: NodeListener<Context>) {
    const node = this.node(path);
    if (!node.listenField) {
      node.listenField = [];
    }
    node.listenField.push(listener);
    return this;
  }

  public alterValue(path: string[], listener: NodeAlter<Context>) {
    const node = this.node(path);
    if (!node.alterValue) {
      node.alterValue = [];
    }
    node.alterValue.push(listener);
    return this;
  }

  public alterField(path: string[], listener: NodeAlter<Context>) {
    const node = this.node(path);
    if (!node.alterField) {
      node.alterField = [];
    }
    node.alterField.push(listener);
    return this;
  }

  public triggerListeners(value: any, context: any = null) {
    iterateNodeField({
      context,
      node: this.root,
      parentValue: null,
      path: [],
      value,
    });
  }

  public triggerAlterators(value: any, context: any = null) {
    let alteredValue = value;

    alterNodeField({
      context,
      node: this.root,
      parentValue: null,
      path: [],
      setValue: newValue => alteredValue = newValue,
      value: alteredValue,
    });

    return alteredValue;
  }

  public node(path: string[]): Node<Context> {
    const generator = nodeGenerator(this.root);
    let node = generator.next().value;
    for (const name of path) {
      node = generator.next(name).value;
    }

    generator.return();
    if (!node) throw new Error(`Path error: ${path.join(', ')}`);

    return node;
  }

  public append(path: string[], schema: JsonSchema<Context>) {
    const node = this.node(path);
    node.appended.push(schema.root);

    return this;
  }
}
