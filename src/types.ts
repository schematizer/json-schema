export interface ListenerArg<Context> {
  path: string[];
  value: any;
  parent: any;
  context: Context;
}
export type NodeListener<Context> = (arg: ListenerArg<Context>) => void;
export type NodeAlter<Context> = (arg: ListenerArg<Context>) => any;
